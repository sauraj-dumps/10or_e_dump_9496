## E-user 8.1.0 OPM1.171019.019 10or_E_V1_0_109 release-keys
- Manufacturer: 10or
- Platform: msm8937
- Codename: E
- Brand: 10or
- Flavor: zql1520-user
- Release Version: 8.1.0
- Id: OPM1.171019.019
- Incremental: 10or_E_V1_0_109
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: false
- Locale: en-IN
- Screen Density: undefined
- Fingerprint: 10or/E/E:8.1.0/OPM1.171019.019/10or_E_V1_0_109:user/release-keys
- OTA version: 
- Branch: E-user-8.1.0-OPM1.171019.019-10or_E_V1_0_109-release-keys
- Repo: 10or_e_dump_9496


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
